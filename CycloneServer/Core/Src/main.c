/**
 * @file main.c
 * @brief Main routine
 *
 * @section License
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 * Copyright (C) 2010-2021 Oryx Embedded SARL. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * @author Oryx Embedded SARL (www.oryx-embedded.com)
 * @version 2.0.2
 **/

//Dependencies
#include <stdlib.h>
#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
#include "stm32f769i_eval.h"
#include "stm32f769i_eval_lcd.h"
#include "core/net.h"
#include "drivers/mac/stm32f7xx_eth_driver.h"
#include "drivers/phy/dp83848_driver.h"
#include "dhcp/dhcp_client.h"
#include "http/http_server.h"
#include "http/mime.h"
#include "web_socket/web_socket.h"
#include "path.h"
#include "date_time.h"
#include "resource_manager.h"
#include "smi_driver.h"
#include "debug.h"
#include "logoUGA.h"
#include "logoGEII.h"
#include "logoPolytech.h"

//LCD frame buffers
#define LCD_FRAME_BUFFER_LAYER0 0xC0400000
#define LCD_FRAME_BUFFER_LAYER1 0xC0480000

//Ethernet interface configuration
#define APP_IF_NAME "eth0"
#define APP_HOST_NAME "HTTP (websocket) Server CAN Sniffer"
#define APP_MAC_ADDR "00-AB-CD-EF-07-69"

//Application configuration
#define APP_HTTP_MAX_CONNECTIONS WEB_SOCKET_MAX_COUNT

//Global variables
uint_t lcdLine = 0;
uint_t lcdColumn = 0;

//CAN structure
CAN_HandleTypeDef hcan1;
CAN_TxHeaderTypeDef TxHeader;
CAN_RxHeaderTypeDef RxHeader;
uint32_t Txmailbox;
uint8_t RxData[8];
uint8_t TxData[8];

//Server structure
DhcpClientSettings dhcpClientSettings;
DhcpClientContext dhcpClientContext;
HttpServerSettings httpServerSettings;
HttpServerContext httpServerContext;
HttpConnection httpConnections[APP_HTTP_MAX_CONNECTIONS];

//Forward declaration of functions
error_t httpServerCgiCallback(HttpConnection *connection, const char_t *param);
error_t httpServerRequestCallback(HttpConnection *connection, const char_t *uri);
error_t httpServerUriNotFoundCallback(HttpConnection *connection,
		const char_t *uri);

//Forward declaration of CAN functions
static void MX_GPIO_Init(void);
static void MX_CAN1_Init(void);
static HAL_StatusTypeDef CAN_Config(void);
void HAL_CAN_MspInit(CAN_HandleTypeDef *hcan);

/*============ Functions ============*/

/**
 * @brief Display a header
 **/
void headerLCD() {
	BSP_LCD_SetTextColor(LCD_COLOR_POLYTECH);
	BSP_LCD_FillRect(0, 0, OTM8009A_800X480_WIDTH, 26);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_SetBackColor(LCD_COLOR_POLYTECH);
	BSP_LCD_DisplayStringAt(0, 5,
			"Embedded HTTP (websocket) Server and CAN Sniffer", CENTER_MODE);
	BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
}

/**
 * @brief Display a footer
 **/
void footerLCD() {
	BSP_LCD_SetTextColor(LCD_COLOR_POLYTECH);
	BSP_LCD_FillRect(0, 410, OTM8009A_800X480_WIDTH, 75);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_SetBackColor(LCD_COLOR_POLYTECH);
	BSP_LCD_DisplayStringAt(15, 417, "Made by Polytech students: (2020-2021)",
			LEFT_MODE);
	BSP_LCD_DisplayStringAt(0, 437,
			"  - INFO4: Liam ANDRIEUX, Lucas DREZET & Roman REGOUIN",
			LEFT_MODE);
	BSP_LCD_DisplayStringAt(0, 457,
			"  - IESE3: Leo BARBET, Nicolas LYSEE & Yoan MOREAU", LEFT_MODE);
	BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
}

/**
 * @brief Display UGA, IUT1 & Polytech Grenoble logos
 **/
void logoLCD() {
	BSP_LCD_DrawBitmap(525, 50, (uint8_t*) logoUGA);
	BSP_LCD_DrawBitmap(525, 170, (uint8_t*) logoGEII);
	BSP_LCD_DrawBitmap(445, 290, (uint8_t*) logoPolytech);
}

/**
 * @brief Display IPv4 address
 * isConnected = 0 => link just went disconnected and need to refresh LCD information by removing the former IPv4 Address and write "CONNECTING..."
 * isConnected = 1 => link just connected and need to refresh LCD information by removing "CONNECTING..." and write the IPv4 Address
 **/
void addrLCD(int *isConnected) {
	NetInterface *interface = &netInterface[0];
	Ipv4Addr ipv4Addr;
	char_t buffer[15];
	char_t displayAddr[26] = "IPv4 Address: ";

	ipv4GetHostAddr(interface, &ipv4Addr);
	ipv4AddrToString(ipv4Addr, buffer);

	BSP_LCD_SetTextColor(LCD_COLOR_UGA);
	if (!strcmp(buffer, "0.0.0.0")) {//If the current IPv4 is "0.0.0.0" (aka not connected)
		if (*isConnected == 0) {//If the link just disconnected, update the display
			BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
			BSP_LCD_FillRect(200, 200, 300, 20);
			BSP_LCD_SetTextColor(LCD_COLOR_UGA);
			BSP_LCD_DisplayStringAt(15, 200, strcat(displayAddr, "CONNECTING..."), LEFT_MODE);
			*isConnected = 1;
		}
	} else if (*isConnected == 1) {//If the current IPv4 Address is != "0.0.0.0" (aka connected) and the link just connected, update the display
		BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
		BSP_LCD_FillRect(200, 200, 300, 20);
		BSP_LCD_SetTextColor(LCD_COLOR_UGA);
		BSP_LCD_DisplayStringAt(15, 200, strcat(displayAddr, buffer), LEFT_MODE);
		*isConnected = 0;
	}
}

/**
 * @brief System clock configuration
 **/
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	//Enable Power Control clock
	__HAL_RCC_PWR_CLK_ENABLE();

	//Enable HSE Oscillator and activate PLL with HSE as source
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 25;
	RCC_OscInitStruct.PLL.PLLN = 400;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 9;
	HAL_RCC_OscConfig(&RCC_OscInitStruct);

	//Enable overdrive mode
	HAL_PWREx_EnableOverDrive();

	//Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
	//clocks dividers
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
	RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
}

/**
 * @brief MPU configuration
 **/
void MPU_Config(void) {
	MPU_Region_InitTypeDef MPU_InitStruct;

	//Disable MPU
	HAL_MPU_Disable();

	//SRAM
	MPU_InitStruct.Enable = MPU_REGION_ENABLE;
	MPU_InitStruct.Number = MPU_REGION_NUMBER0;
	MPU_InitStruct.BaseAddress = 0x20000000;
	MPU_InitStruct.Size = MPU_REGION_SIZE_512KB;
	MPU_InitStruct.SubRegionDisable = 0;
	MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;
	MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
	MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
	MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
	HAL_MPU_ConfigRegion(&MPU_InitStruct);

	//SRAM2
	MPU_InitStruct.Enable = MPU_REGION_ENABLE;
	MPU_InitStruct.Number = MPU_REGION_NUMBER1;
	MPU_InitStruct.BaseAddress = 0x2007C000;
	MPU_InitStruct.Size = MPU_REGION_SIZE_16KB;
	MPU_InitStruct.SubRegionDisable = 0;
	MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;
	MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
	MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
	MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
	HAL_MPU_ConfigRegion(&MPU_InitStruct);

	//SDRAM
	MPU_InitStruct.Enable = MPU_REGION_ENABLE;
	MPU_InitStruct.Number = MPU_REGION_NUMBER2;
	MPU_InitStruct.BaseAddress = 0xC0000000;
	MPU_InitStruct.Size = MPU_REGION_SIZE_8MB;
	MPU_InitStruct.SubRegionDisable = 0;
	MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;
	MPU_InitStruct.IsShareable = MPU_ACCESS_NOT_SHAREABLE;
	MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
	MPU_InitStruct.IsBufferable = MPU_ACCESS_BUFFERABLE;
	HAL_MPU_ConfigRegion(&MPU_InitStruct);

	//LCD frame buffer
	MPU_InitStruct.Enable = MPU_REGION_ENABLE;
	MPU_InitStruct.Number = MPU_REGION_NUMBER3;
	MPU_InitStruct.BaseAddress = 0xC0400000;
	MPU_InitStruct.Size = MPU_REGION_SIZE_4MB;
	MPU_InitStruct.SubRegionDisable = 0;
	MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
	MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
	MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;
	MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
	MPU_InitStruct.IsCacheable = MPU_ACCESS_CACHEABLE;
	MPU_InitStruct.IsBufferable = MPU_ACCESS_NOT_BUFFERABLE;
	HAL_MPU_ConfigRegion(&MPU_InitStruct);

	//Enable MPU
	HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
 * @brief CAN INIT Peripheral
 **/
static void MX_CAN1_Init(void) {
	hcan1.Instance = CAN1;
	hcan1.Init.Prescaler = 5;
	hcan1.Init.Mode = CAN_MODE_NORMAL;
	hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
	hcan1.Init.TimeSeg1 = CAN_BS1_5TQ;
	hcan1.Init.TimeSeg2 = CAN_BS2_4TQ;
	hcan1.Init.TimeTriggeredMode = DISABLE;
	hcan1.Init.AutoBusOff = DISABLE;
	hcan1.Init.AutoWakeUp = DISABLE;
	hcan1.Init.AutoRetransmission = DISABLE;
	hcan1.Init.ReceiveFifoLocked = DISABLE;
	hcan1.Init.TransmitFifoPriority = DISABLE;
	if (HAL_CAN_Init(&hcan1) != HAL_OK)
		TRACE_ERROR("CAN Init error!\r\n");
}

/**
 * @brief CAN INIT Pins
 **/
void HAL_CAN_MspInit(CAN_HandleTypeDef *hcan) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	if (hcan->Instance == CAN1) {
		/* Peripheral clock enable */
		__HAL_RCC_CAN1_CLK_ENABLE();

		__HAL_RCC_GPIOA_CLK_ENABLE();
		/**CAN1 GPIO Configuration
		 PA12     ------> CAN1_TX
		 PA11     ------> CAN1_RX
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_12 | GPIO_PIN_11;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF9_CAN1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	}
}

/**
 * @brief GPIO CLOCK INIT
 **/
static void MX_GPIO_Init(void) {

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
}

/**
 * @brief CAN CONFIG FILTERS
 **/
HAL_StatusTypeDef CAN_Config(void) {
	CAN_FilterTypeDef sFilterConfig;
	/*#-2- Configure the CAN Filter #*/
	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	if (HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK)
		TRACE_ERROR("CAN filter config error!\r\n");

	/*#-3- Start the CAN peripheral #*/
	if (HAL_CAN_Start(&hcan1) != HAL_OK)
		TRACE_ERROR("CAN start error!\r\n");

	return HAL_OK;
}

/**
 * @brief CGI callback function
 * @param[in] connection Handle referencing a client connection
 * @param[in] param NULL-terminated string that contains the CGI parameter
 * @return Error code
 **/
error_t httpServerCgiCallback(HttpConnection *connection, const char_t *param) {
	//Not implemented
	return ERROR_NOT_FOUND;
}

/**
 * @brief HTTP request callback
 * @param[in] connection Handle referencing a client connection
 * @param[in] uri NULL-terminated string containing the path to the requested resource
 * @return Error code
 **/
error_t httpServerRequestCallback(HttpConnection *connection, const char_t *uri) {
	//If the connection is not yet a websocket one and the connection asks for an upgrade
	if (connection->socket != NULL && connection->request.upgradeWebSocket) {
		//Upgrade the connection (client one)
		WebSocket *ws = httpUpgradeToWebSocket(connection);
		//Validate the upgrade (server one)
		error_t error = webSocketSendServerHandshake(ws);
		if (error == NO_ERROR)
			TRACE_INFO("Websocket connection established!\r\n");
		else
			TRACE_ERROR("Websocket connection failed!\r\n");
	}

	//The requested resource cannot be found
	return ERROR_NOT_FOUND;
}

/**
 * @brief URI not found callback
 * @param[in] connection Handle referencing a client connection
 * @param[in] uri NULL-terminated string containing the path to the requested resource
 * @return Error code
 **/
error_t httpServerUriNotFoundCallback(HttpConnection *connection,
		const char_t *uri) {
	//Not implemented
	return ERROR_NOT_FOUND;
}

/**
 * @brief send data from CAN bus to client
 **/
void sendCANBusToClient() {
	char_t buffer[128];
	//Format XML data
	int n = sprintf(buffer, "<data>\r\n");
	n += sprintf(buffer + n, "<id>%x</id>\r\n", RxHeader.StdId);
	n += sprintf(buffer + n, "<dlc>%d</dlc>\r\n", RxHeader.DLC);
	n += sprintf(buffer + n, "<B1>%x</B1>\r\n", RxData[0]);
	n += sprintf(buffer + n, "<B2>%x</B2>\r\n", RxData[1]);
	n += sprintf(buffer + n, "</data>\r\n");

	//Send this data to all open connections (websockets)
	for (int i = 0; i < WEB_SOCKET_MAX_COUNT; i++) {
		WebSocket *ws = &webSocketTable[i];
		if (ws->state == WS_STATE_OPEN) {
			error_t error = webSocketSend(ws, buffer, n, WS_FRAME_TYPE_TEXT, 0);//Send the xml through websocket connection
			if (error == ERROR_NOT_CONNECTED)//If an error occurred, close the websocket connection
				webSocketClose(ws);
		}
	}
}

/**
 * @brief send data from client to CAN bus or close connection
 **/
void parseMessage(char_t *buffer, size_t size, WebSocket *ws) {
	/*buffer's structure is:
	 <data>
	 <id>xx</id>
	 <B1>xx</B1>
	 <B2>xx</B2>
	 </data>

	 or
	 close
	 */
	if (size == 5)//size = 5 => client closed the connection so close it here too (not really needed but accelerate the process of stopping a websocket connection)
		webSocketClose(ws);
	else {
		char_t *delim = "<>\n";	//token we want ignore
		char_t *c = strtok(buffer, delim);//pointer right after the first delimiter
		c = strtok(NULL, delim);	//do it again
		c = strtok(NULL, delim);	//and again to reach the 1st value: id
		uint32_t stdid = (uint32_t) strtol(c, NULL, 10);//parse it from decimal to hexa

		//and again for the 1st byte
		c = strtok(NULL, delim);
		c = strtok(NULL, delim);
		c = strtok(NULL, delim);
		TxData[0] = (uint8_t) strtol(c, NULL, 10);

		//and finally the 2nd byte
		c = strtok(NULL, delim);
		c = strtok(NULL, delim);
		c = strtok(NULL, delim);
		TxData[1] = (uint8_t) strtol(c, NULL, 10);

		TxHeader.StdId = stdid;
		TxHeader.DLC = 2;	//we manipulate only input so dlc = 2

		//check if no messages are pending
		if (HAL_CAN_IsTxMessagePending(&hcan1, Txmailbox) == 0) {
			//send the message to the CAN bus
			if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &Txmailbox)
					!= HAL_OK) {
				//transmission Error
				TRACE_ERROR("CAN transmission error!\r\n");
			}
		}
	}
}

/*============ Tasks ============*/

/**
 * @brief LCD task
 * @param[in] param Unused parameter
 **/
void lcdTask(void *param) {
	int isConnected = 0;

	//Endless loop
	while (1) {
		addrLCD(&isConnected);

		//Loop delay
		osDelayTask(10);
	}
}

/**
 * @brief Message received from CAN bus and sent to client
 * @param[in] param Unused parameter
 **/
void canTask(void *param) {
	HAL_StatusTypeDef status;
	while (1) {
		//Checks if message is pending in the RX FIFO 0
		if (HAL_CAN_GetRxFifoFillLevel(&hcan1, CAN_RX_FIFO0) != 0) {
			//Puts message in RxHeader
			if (HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &RxHeader, RxData)
					!= HAL_OK) {
				//Reception Error
				TRACE_ERROR("CAN reception error!\r\n");
			} else {
				if ((RxHeader.StdId >= 0x182 && RxHeader.StdId <= 0x186)
						|| (RxHeader.StdId >= 0x202 && RxHeader.StdId <= 0x206))//We take only CAN messages which are interesting
					sendCANBusToClient();//We send same to all websocket connections
			}
		}
		osDelayTask(10);
	}
}

/**
 * @brief Message received from client and sent to CAN bus
 * @param[in] param Unused parameter
 **/
void clientTask(void *param) {
	error_t error;
	WebSocketFrameType *typeFrame = malloc(sizeof(WebSocketFrameType*));
	size_t *sizeRead = malloc(sizeof(size_t*));
	while (1) {
		//Receive data from all open connections (websockets)
		for (int i = 0; i < WEB_SOCKET_MAX_COUNT; i++) {
			WebSocket *ws = &webSocketTable[i];
			if (ws->state == WS_STATE_OPEN) {
				char_t buffer[128];
				error = webSocketReceive(ws, buffer, 128, typeFrame, sizeRead);//We take a message from a client
				if (error == ERROR_NOT_CONNECTED)//If an error occurred, close the websocket connection
					webSocketClose(ws);
				else if (*sizeRead != 0) {//If the size of the message is > 0 (aka we received a message), we parse it
					parseMessage(buffer, *sizeRead, ws);
				}
			}
		}
		osDelayTask(10);
	}
}

/*============ Main ============*/

/**
 * @brief Main entry point
 * @return Unused value
 **/
int_t main(void) {
	error_t error;
	OsTask *task;
	NetInterface *interface;
	MacAddr macAddr;

	//MPU configuration
	MPU_Config();
	//HAL library initialization
	HAL_Init();
	//Configure the system clock
	SystemClock_Config();

	//Enable I-cache and D-cache
	SCB_EnableICache();
	SCB_EnableDCache();

	//Initialize kernel
	osInitKernel();
	//Configure debug UART
	debugInit(115200);

	//Start-up message
	TRACE_INFO("\r\n");
	TRACE_INFO("******************************************\r\n");
	TRACE_INFO("*** CycloneTCP HTTP (websocket) Server ***\r\n");
	TRACE_INFO("******************************************\r\n");
	TRACE_INFO("Copyright: 2010-2021 Oryx Embedded SARL\r\n");
	TRACE_INFO("Compiled: %s %s\r\n", __DATE__, __TIME__);
	TRACE_INFO("Target: STM32F779\r\n");
	TRACE_INFO("\r\n");

	//LED configuration
	BSP_LED_Init(LED1);
	BSP_LED_Init(LED2);
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);

	//Clear LEDs
	BSP_LED_Off(LED1);
	BSP_LED_Off(LED2);
	BSP_LED_Off(LED3);
	BSP_LED_Off(LED4);

	//Initialize LCD display
	BSP_LCD_Init();
	BSP_LCD_LayerDefaultInit(0, LCD_FRAME_BUFFER_LAYER0);
	BSP_LCD_SelectLayer(0);
	BSP_LCD_DisplayOn();
	BSP_LCD_SetBrightness(100);
	BSP_LCD_Clear(LCD_COLOR_WHITE);
	BSP_LCD_SetFont(&Font20);

	//Initialize display
	headerLCD();
	footerLCD();
	logoLCD();

	//Initialize CAN
	MX_GPIO_Init();
	MX_CAN1_Init();
	CAN_Config();

	//TCP/IP stack initialization
	error = netInit();
	//Any error to report?
	if (error) {
		//Debug message
		TRACE_ERROR("Failed to initialize TCP/IP stack!\r\n");
	}

	//Configure the first Ethernet interface
	interface = &netInterface[0];

	//Set interface name
	netSetInterfaceName(interface, APP_IF_NAME);
	//Set host name
	netSetHostname(interface, APP_HOST_NAME);
	//Set host MAC address
	macStringToAddr(APP_MAC_ADDR, &macAddr);
	netSetMacAddr(interface, &macAddr);
	//Select the relevant network adapter
	netSetDriver(interface, &stm32f7xxEthDriver);
	netSetPhyDriver(interface, &dp83848PhyDriver);
	//MDIO and MDC are managed by software (default jumper configuration)
	netSetSmiDriver(interface, &smiDriver);

	//Initialize network interface
	error = netConfigInterface(interface);
	//Any error to report?
	if (error) {
		//Debug message
		TRACE_ERROR("Failed to configure interface %s!\r\n", interface->name);
	}

	//Get default settings
	dhcpClientGetDefaultSettings(&dhcpClientSettings);
	//Set the network interface to be configured by DHCP
	dhcpClientSettings.interface = interface;
	//Disable rapid commit option
	dhcpClientSettings.rapidCommit = FALSE;

	//DHCP client initialization
	error = dhcpClientInit(&dhcpClientContext, &dhcpClientSettings);
	//Failed to initialize DHCP client?
	if (error) {
		//Debug message
		TRACE_ERROR("Failed to initialize DHCP client!\r\n");
	}

	//Start DHCP client
	error = dhcpClientStart(&dhcpClientContext);
	//Failed to start DHCP client?
	if (error) {
		//Debug message
		TRACE_ERROR("Failed to start DHCP client!\r\n");
	}

	//Get default settings
	httpServerGetDefaultSettings(&httpServerSettings);
	//Bind HTTP server to the desired interface
	httpServerSettings.interface = &netInterface[0];
	//Listen to port 80
	httpServerSettings.port = HTTP_PORT;
	//Client connections
	httpServerSettings.maxConnections = APP_HTTP_MAX_CONNECTIONS;
	httpServerSettings.connections = httpConnections;
	//Specify the server's root directory
	strcpy(httpServerSettings.rootDirectory, "/www/");
	//Set default home page
	strcpy(httpServerSettings.defaultDocument, "index.html");
	//Callback functions
	httpServerSettings.cgiCallback = httpServerCgiCallback;
	httpServerSettings.requestCallback = httpServerRequestCallback;
	httpServerSettings.uriNotFoundCallback = httpServerUriNotFoundCallback;

	//HTTP server initialization
	error = httpServerInit(&httpServerContext, &httpServerSettings);
	//Failed to initialize HTTP server?
	if (error) {
		//Debug message
		TRACE_ERROR("Failed to initialize HTTP server!\r\n");
	}

	//Start HTTP server
	error = httpServerStart(&httpServerContext);
	//Failed to start HTTP server?
	if (error) {
		//Debug message
		TRACE_ERROR("Failed to start HTTP server!\r\n");
	}

	//Create lcd task
	task = osCreateTask("LCD", lcdTask, NULL, 500, OS_TASK_PRIORITY_NORMAL);
	//Failed to create the task?
	if (task == OS_INVALID_HANDLE) {
		//Debug message
		TRACE_ERROR("Failed to create lcdTask!\r\n");
	}

	//Create a task to communicate in CAN bus
	task = osCreateTask("CAN Received", canTask, NULL, 400,
	OS_TASK_PRIORITY_HIGH);
	//Failed to create the task?
	if (task == OS_INVALID_HANDLE) {
		//Debug message
		TRACE_ERROR("Failed to create canTask!\r\n");
	}

	//Create a task to communicate in CAN bus
	task = osCreateTask("Client Received", clientTask, NULL, 400,
	OS_TASK_PRIORITY_HIGH);
	//Failed to create the task?
	if (task == OS_INVALID_HANDLE) {
		//Debug message
		TRACE_ERROR("Failed to create recvTask!\r\n");
	}

	//Start the execution of tasks
	osStartKernel();

	//This function should never return
	return 0;
}
