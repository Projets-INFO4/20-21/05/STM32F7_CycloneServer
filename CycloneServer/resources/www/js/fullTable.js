const arrayHeadingFull = ["Data Pack", "Time", "Id", "Dlc", "Data"];
let arrayDataFull = [];
let arrayCounter = 0;
let colFull = 5;
let subColData = 2;
let defaultFilter = null;
let filters = [];
let viewCounter = 0;
let firstPacket = true;

function loadFull() {
  supprFullTable();
  createFullTable();
  initiateFull();
  initiateFilters();
  cleanSearchBar();
};

function initiateFilters() {
  for (let i = 0; i < colFull; i++)
    filters[i] = null;
}

function initiateFull() {
  arrayDataFull = [];
  arrayCounter = 0;
  defaultFilter = null;
  viewCounter = 0;
  firstPacket = true;
}

function cleanSearchBar() {
  let search = document.getElementById("search");
  search.value = "";
}

function addHeadingFull() {
  let container = document.getElementById("tableContainer");
  let table = document.getElementById("fullTable");
  container.append(table);
  let tableRow = document.createElement("tr");
  let itemCol = null;
  for (let i = 0; i < colFull; i++) {
    itemCol = document.createElement("th");
    itemCol.scope = "col";
    itemCol.innerText = arrayHeadingFull[i];
    if (i == 4) {
      itemCol.colSpan = 2;
    } else {
      itemCol.rowSpan = 2;
    }
    tableRow.appendChild(itemCol);
  }

  table.appendChild(tableRow);
  tableRow = document.createElement("tr");
  for (let i = 1; i < 3; i++) {
    itemCol = document.createElement("th");
    itemCol.scope = "col";
    itemCol.innerText = "Byte " + i;
    tableRow.appendChild(itemCol);
  }
  table.appendChild(tableRow);
}

//Table operation function

// delete the table with the div given on the html page
function supprFullTable() {
  let tmp = document.getElementById("fullTable");
  if (tmp != undefined)
    tmp.parentNode.removeChild(tmp);
}

// function to create html tags for the table
function createFullTable() {
  let table = document.createElement("table");
  table.id = "fullTable";
  document.getElementById("fullTableContainer").appendChild(table);
  addHeadingFull();
}

// if no filters it add the item in the html table by calling addToFullTable
// if filters, check if the item match the corresponding filter
// to know if it needs to be added to the table
function addFullTable(item) {
  let none = true;
  if (defaultFilter != null) {
    let filt = new String(defaultFilter);
    for (let i = 0; i < colFull; i++) {
      let s;
      if(i==4){
        for(let k=0;k<subColData;k++){
          s=item[i][k];
          let s1="";
          for(let j=0;j<8-s.toString(2).length;j++)s1+="0";
          s1+=s.toString(2);
          if(s.toString(16).indexOf(filt) != -1 || s1.indexOf(filt) != -1){
            addToFullTable(item);
            return;
          }
        }
        break;
      }else s = new String(item[i]);
      
      let s1="-1";
      if(i==2){
        s1="";
        for(let j=0;j<12-parseInt(s).toString(2).length;j++)s1+="0";
        s1+=parseInt(s).toString(2);
      }
      if (s.indexOf(filt) != -1 || s1.indexOf(filt) != -1) {
        addToFullTable(item);
        return;
      }
    }
    return;
  }
  for (let i = 0; i < colFull; i++) {
    if (filters[i] != null) {
      none = false;
      if (i == 4) {
        // data filter
        let search = filters[i].search;
        let searchBase = filters[i].searchBase;
        let currentByte = item[i][filters[i].numByte - 1]; // always in hexadecimal
        if (currentByte != null) {
          if (searchBase === "hexa") {
            if (
              currentByte.toString(16).toUpperCase() == search.toUpperCase()
            ) {
              addToFullTable(item);
            }
          } else if (searchBase === "bin") {
            if (currentByte.toString(2).toUpperCase() == search.toUpperCase()) {
              addToFullTable(item);
            }
          }
        }
      } else {
        if (filters[i] == item[i]) addToFullTable(item);
      }
    }
  }
  if (none) addToFullTable(item);
}

// the actual function that add the data to the html table
function addToFullTable(data) {
  let tableRow = document.createElement("tr");
  if (viewCounter % 2 === 0) {
    tableRow.style.backgroundColor = "rgba(133,156,255,0.5)";
  } else {
    tableRow.style.backgroundColor = "white";
  }

  let itemCol = null;
  for (let i = 0; i < colFull; i++) {
    if (i == 4) {
      for (let j = 0; j < subColData; j++) {
        let cell = document.createElement("td");
        if (data[i][j] == null) {
          cell.innerText = "---";
        } else {
          if (!showInHexa) {
            cell.innerText = hexaToBin(data[i][j].toString(2).toUpperCase(), 8);
          } else {
            cell.innerText = data[i][j].toString(16).toUpperCase();
          }
        }
        tableRow.appendChild(cell);
      }
    } else {
      itemCol = document.createElement("td");
      if (i == 2) {
        if (!showInHexa) itemCol.innerText = hexaToBin(data[i].toString(2).toUpperCase(), 12);
        else itemCol.innerText = data[i].toString(16).toUpperCase();
      } else itemCol.innerText = data[i];
      tableRow.appendChild(itemCol);
    }
  }
  let table = document.getElementById("fullTable");
  table.appendChild(tableRow);
  viewCounter++;
}

// Reload the table
// This function is called to allow to refresh data received or apply filters
function reloadFullTable() {
  supprFullTable();
  createFullTable();
  viewCounter = 0;
  arrayDataFull.forEach((item) => {
    addFullTable(item);
  });
}

//Filter functions

// Resest all filters
function resetFilters() {
  defaultFilter = null;
  for (let i = 0; i < colFull; i++) {
    filters[i] = null;
  }
}

// apply the filter given from the web page
// filters can be field==value to search for a specific value in a field
// if no comparaison is done it search for a match of the given value in the fields
function filter(value) {
  let s = new String(value);
  s = s.toLowerCase();
  let index = s.indexOf("==");

  switch (s.substring(0, index)) {
    case "datapack":
      resetFilters();
      filters[0] = s.substring(index + 2);
      reloadFullTable();
      break;
    case "time":
      resetFilters();
      filters[1] = s.substring(index + 2);
      reloadFullTable();
      break;
    case "id":
      resetFilters();
      filters[2] = s.substring(index + 2);
      reloadFullTable();
      break;
    case "dlc":
      resetFilters();
      filters[3] = s.substring(index + 2);
      reloadFullTable();
      break;
    case "byte1":
    case "byte2":
      resetFilters();
      let nbByte = s.substring(4, 5);
      if (s.substring(index + 2, index + 4) == "0x") {
        // the user write in hexa
        filters[4] = {
          numByte: nbByte,
          search: s.substring(index + 4),
          searchBase: "hexa",
        };
      } else {
        // the user search in binary
        if (showInHexa)
          filters[4] = {
            numByte: nbByte,
            search: s.substring(index + 2),
            searchBase: "hexa",
          };
        else
          filters[4] = {
            numByte: nbByte,
            search: s.substring(index + 2),
            searchBase: "bin",
          };
      }
      reloadFullTable();
      return;

    default:
      if (index == -1) {
        if (s.length == 0) {
          resetFilters();
          reloadFullTable();
        } else {
          resetFilters();
          defaultFilter = s;
          reloadFullTable();
        }
        break;
      }
      for (let i = 0; i < colFull; i++) {
        if (filters[i] != null) {
          filters[i] = null;
          reloadFullTable();
        }
      }
      break;
  }
}
