window.addEventListener("load", function () {
  initLastData();
});

// Associative array to get corresponding address with the actual floor
// If you add an floor to the elevator, it needs to be added here with the corresponding address
const getAddresses = {
  0: 6,
  1: 5,
  2: 4,
  3: 3,
  4: 2,
};

// Associative array to get corresponding floor with the actual address
// If you add an addres to the elevator, it needs to be added here with the
const getFloor = {
  2: 4,
  3: 3,
  4: 2,
  5: 1,
  6: 0,
};

// Associative array to get corresponding address depending on input/output data
const defaultCANId = {
  input: 384,
  output: 512,
};

const maxBit = 12; // the higher bit that we need (going from 0 to 11)
const nbFloor = 5; // going from 0 to nbFloor-1
let lastDataInput = {
  0: {},
  1: {},
  2: {},
  3: {},
  4: {},
};

let lastDataOutput = {
    0: {},
    1: {},
    2: {},
    3: {},
    4: {},
  };

function changeIndicator(floor, currentState, lastState, id) {
  let src = null;
  let indicator = document.getElementById(id);
  if (currentState != lastState){
    if (currentState == 1) src = "IndicatorON";
    else src = "indicatorOFF";
    addImage(indicator, src, indicator.alt + floor, "png", indicator.id);
  }
}

function changeButton(floor, currentState, lastState, id) {
  let src = null;
  let button = document.getElementById(id);
  if (currentState!=lastState) {
      if (currentState == 1)
          src = "BpON";
      else
          src = "BpOFF";
      addImage(button, src, button.alt + floor, "png", button.id);
  }
}

function initLastData() {
  for (let k = 0; k < nbFloor; k++)
    for (let i = 0; i < maxBit; i++) {
      if(i==3)lastDataInput[k][i] = 1;
      else lastDataInput[k][i] = 0;
      if(i<8)lastDataOutput[k][i] = 0;
    }
  lastDataInput[0][0]==1;
}


function pressedButtonOnFloor(floor){
    lastDataInput[floor][5] = 1;
    data = recreateData(floor);
    lastDataInput[floor][5] = 0;
    transferData(data,true);
    addFullTable(data);
    addShortTable(data);
    interpretData(data);
    setTimeout(() => {
      lastDataInput[floor][5] = 0;
      data = recreateData(floor);
      lastDataInput[floor][5] = 1;
      transferData(data);
      addFullTable(data);
      addShortTable(data);
      interpretData(data);
    },200)
}


function pressedButtonCabin(nbFloorPressed){
  if(nbFloorPressed==4)lastDataInput[3][6] = 1;
  else lastDataInput[3][8+nbFloorPressed] = 1;
  data = recreateData(3);
  if(nbFloorPressed==4)lastDataInput[3][6] = 0;
  else lastDataInput[3][8+nbFloorPressed] = 0;
  transferData(data,true);
  addFullTable(data);
  addShortTable(data);
  interpretData(data);
  setTimeout(() => {
    if(nbFloorPressed==4)lastDataInput[3][6] = 0;
    else lastDataInput[3][8+nbFloorPressed] = 0;
    data = recreateData(3);
    if(nbFloorPressed==4)lastDataInput[3][6] = 1;
    else lastDataInput[3][8+nbFloorPressed] = 1;
    transferData(data);
    addShortTable(data);
    addFullTable(data);
    interpretData(data);
  },200)
}

function recreateData(floor){
  let data=[];

  data[0] = arrayCounter+1;

  let time = "" + Date.now() - dateFirstPacket + " ms";
  data[1] = time;

  data[2] = defaultCANId["input"] + getAddresses[floor];

  data[3] = 2;

  let byte1 = 0;
  let byte2 = 0;
  for (let i=0; i<8; i++) {
    byte1 += lastDataInput[floor][i]*2**i;
    if(i<4) byte2 += lastDataInput[floor][i+8]*2**i;
  }
  
  data[4] = [byte1,byte2];
  arrayDataFull[arrayCounter] = data;
  arrayCounter++;
  document.getElementById("counterShow").innerText =
    "Number of data pack received : " + arrayCounter;
  return data;
}


function sensorFloor(floor, currentState,lastState) {
  if(currentState==lastState)return;
  changeIndicator(floor, currentState, lastState, "indicatorF" + floor);
  if(currentState==1)checkAnimationCabin(floor,0);
}

function upSensorFloor(floor, currentState,lastState) {
  if(currentState==lastState)return;
  if (floor == 0) return;
  changeIndicator(floor, currentState,lastState, "indicatorUF" + floor);
}

function downSensorFloor(floor, currentState,lastState) {
  if(currentState==lastState)return;
  if (floor == 0) return;
  changeIndicator(floor, currentState,lastState, "indicatorDF" + floor);
  if(currentState==1)checkAnimationCabin(floor,-1);
}

function openDoors(floor,currentState,lastState) {
  if(currentState==lastState || currentState==0 || currentState!=1)return;
  let door = document.getElementById("imageDoor" + floor);
  addImage(door, "DoorOPEN", door.alt + floor, "jpg", door.id);
  
}

function closeDoors(floor, currentState,lastState) {
  if(currentState==lastState || currentState==0 || currentState!=1)return;
  let door = document.getElementById("imageDoor" + floor);
  addImage(door, "DoorCLOSE", door.alt + floor, "jpg", door.id);
}

function buttonFloor(floor, currentState,lastState) {
  changeButton(floor,currentState,lastState,"imagePressButtonFloor" + floor);
}

function cabinButtonFloor0(floor, currentState,lastState) {
  if (floor != 3) return;
  changeButton(floor, currentState, lastState, "buttonCabin0");
}

function cabinButtonFloor1(floor, currentState,lastState) {
  if (floor != 3) return;
  changeButton(floor, currentState, lastState, "buttonCabin1");
}

function cabinButtonFloor2(floor, currentState,lastState) {
  if (floor != 3) return;
  changeButton(floor, currentState, lastState, "buttonCabin2");
}

function cabinButtonFloor3(floor, currentState,lastState) {
  changeButton(floor, currentState, lastState, "buttonCabin3");
  if (floor != 3) return;
}

function cabinButtonFloor4(floor, currentState,lastState) {
  changeButton(floor, currentState, lastState, "buttonCabin4");
  if (floor != 3) return;
}

// Fonction corresponding to the bit in the packed
// The treatment of the potential changes of data needs to be done in the fonction
const inputFunction = {
  0:sensorFloor,
  1:upSensorFloor,
  2:downSensorFloor,
  3:closeDoors,
  4:openDoors,
  5:buttonFloor,
  6:cabinButtonFloor4,
  7:function(){return;},
  8:cabinButtonFloor0,
  9:cabinButtonFloor1,
  10:cabinButtonFloor2,
  11:cabinButtonFloor3
};

function indicatorCall0(floor, currentState, lastState) {
  if (floor != 0) return;
  changeIndicator(floor, currentState, lastState, "drcIndicator0");
}

function indicatorCall1(floor, currentState, lastState) {
  if (floor != 0) return;
  changeIndicator(floor, currentState, lastState, "drcIndicator1");
}

function indicatorCall2(floor, currentState, lastState) {
  if (floor != 0) return;
  changeIndicator(floor, currentState, lastState, "drcIndicator2");
}

function indicatorCall3(floor, currentState, lastState) {
  if (floor != 0) return;
  changeIndicator(floor, currentState, lastState, "drcIndicator3");
}

function indicatorCall4(floor, currentState, lastState) {
  if (floor != 0) return;
  changeIndicator(floor, currentState, lastState, "drcIndicator4");
}

function doorOpeningDirection(floor, currentState,lastState) {
  if(currentState==lastState)return;
  let direction = document.getElementById("imageDirection" + floor);
  if (currentState == 1)
    addImage(direction, "OPEN", direction.alt + floor, "png", direction.id);
  else
    addImage(direction, "CLOSE", direction.alt + floor, "png", direction.id);
}

function doorMoving(floor, currentState,lastState) {
  //Nothing to do in our UI
  //If you want to do special thing when the engine of the door is in action
}

function indicatorFloor(floor, currentState,lastState) {
  if(currentState==lastState)return;
  changeIndicator(floor, currentState,lastState, "indicatorBP" + floor);
}

const outputFunction = {
  0:indicatorCall0,
  1:indicatorCall1,
  2:indicatorCall2,
  3:indicatorCall3,
  4:indicatorCall4,
  5:doorOpeningDirection,
  6:doorMoving,
  7:indicatorFloor,
};

// Interpret incoming data and apply with the corresponding floor and input argument
// If
function interpretData(data) {
  let floor;
  if ((floor = getFloor[data[2] - defaultCANId["input"]]) != undefined)
    interpretDataFloor(data[4], floor, true);
  else if ((floor = getFloor[data[2] - defaultCANId["output"]]) != undefined)
    interpretDataFloor(data[4], floor, false);
}

function interpretDataFloor(data, floor, input) {
  let bits =  data[1].toString(2).substring();
  let tmp = data[0].toString(2);
  for(let i=0;i<8-tmp.length;i++) bits+="0";
  bits += tmp;
  let state = 0;
  for (let i = 0; i < maxBit; i++) {
    if(i<bits.length)state=parseInt(bits.substring(bits.length-1-i,bits.length-i));
    else state=0;
    if (input) {
      inputFunction[i].apply(null, [floor,state, lastDataInput[floor][i]]);
    } else {
      if (i >= 8)
        break; // Never go further than 8 bits for output
      outputFunction[i].apply(null, [floor, state, lastDataOutput[floor][i]]);
    }
  }
  refreshData(floor, bits,input);
}

// Refresh lastData for a floor with the data given
function refreshData(floor, bits,input) {
  let state = 0;
  for (let i = 0; i < maxBit; i++) {
    if(i<bits.length) state = parseInt(bits.substring(bits.length-1-i,bits.length-i));
    else state = 0;
    if(input)lastDataInput[floor][i] = state;
    else lastDataOutput[floor][i] = state;
  }
}

function transferData(data) {
  let xml = "<data>\n";
  xml += "<id>" + data[2] + "</id>\n";
  xml += "<B1>" + data[4][0] + "</B1>\n";
  xml += "<B2>" + data[4][1] + "</B2>\n";
  xml += "</data>";

  socket.send(xml);
}
