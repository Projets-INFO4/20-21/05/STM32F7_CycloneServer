let parser = new DOMParser();
let dateFirstPacket=0;
let showInHexa = true;

window.addEventListener("load", function () {
  newWebSocket();
  dateFirstPacket = Date.now();
  loadFull();
  loadShort();
});

function dataReceived(recvData) {
  xmlDoc = parser.parseFromString(recvData, "text/xml");

  id = parseInt(
    xmlDoc.getElementsByTagName("id")[0].childNodes[0].nodeValue,
    16
  );
  dlc = parseInt(xmlDoc.getElementsByTagName("dlc")[0].childNodes[0].nodeValue);
  B1 = parseInt(
    xmlDoc.getElementsByTagName("B1")[0].childNodes[0].nodeValue,
    16
  );
  B2 = parseInt(
    xmlDoc.getElementsByTagName("B2")[0].childNodes[0].nodeValue,
    16
  );

  
  let time = "" + Date.now() - dateFirstPacket + " ms";

  let data = [arrayCounter + 1, time, id, dlc, [B1, B2]];
  arrayDataFull[arrayCounter] = data;
  arrayCounter += 1;

  document.getElementById("counterShow").innerText =
    "Number of data pack received : " + arrayCounter;
  addFullTable(data);
  addShortTable(data);
  interpretData(data);
}

function switchBase() {
  showInHexa = !showInHexa;
  let menuItem = document.getElementById("hexaBinButton");
  if (showInHexa) {
    menuItem.innerText = "Convert to binary";
  } else {
    menuItem.innerText = "Convert to hexa";
  }
  reloadFullTable();
  reloadShortTable();
}

function goToAnchor(anchor) {
  let location = document.location.toString().split('#')[0];
  document.location = location + anchor;
}

function hexaToBin(str, nb) {
  let val = "";
  let missingBit = nb - str.length;
  let j = 0;
  for (let i = 0; i != nb; i++) {
    if (i < missingBit)
      val += "0";
    else
      val += str[j++];
    if (i == 3 || i == 7)
      val += " ";
  }
  return val;
}