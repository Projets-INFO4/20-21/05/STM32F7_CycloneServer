let cptImg = 0;

function openModal() {
    let modal = document.getElementById("helpModal");
    let modalImg = document.getElementById("helpImg");
    modal.style.display = "block";
    modalImg.src = "/images/helpNavbar.jpg";
}

function closeModal() {
  let modal = document.getElementById("helpModal");
  modal.style.display = "none";
}

function switchImg(next) {
  let modalImg = document.getElementById("helpImg");
  if (next)
    cptImg++;
  else
    cptImg--;
  if (Math.abs(cptImg) % 4 == 0) {
    modalImg.src = "/images/helpNavbar.jpg"
  } else if (Math.abs(cptImg) % 4 == 1) {
    modalImg.src = "/images/helpSketch.jpg"
    goToAnchor("#sketchView");
  } else if (Math.abs(cptImg) % 4 ==2) {
    modalImg.src = "/images/helpFull.jpg"
    goToAnchor("#fullTableView");
  } else if (Math.abs(cptImg) % 4 == 3) {
    modalImg.src = "/images/helpShort.jpg"
    goToAnchor("#shortTableView");
  }
}