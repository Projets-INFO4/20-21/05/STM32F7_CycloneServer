const arrayHeadingShort = ["Floor", "Id", "Dlc", "Data"];
let arrayDataShort;
let colShort = 4;
const getFloorID = {
  2: 4,
  3: 3,
  4: 2,
  5: 1,
  6: 0,
};

function loadShort() {
  arrayDataShort = {
    4: {
      input: [0, 0, [0, 0]],
      output: [0, 0, [0, 0]],
    },
    3: {
      input: [0, 0, [0, 0]],
      output: [0, 0, [0, 0]],
    },
    2: {
      input: [0, 0, [0, 0]],
      output: [0, 0, [0, 0]],
    },
    1: {
      input: [0, 0, [0, 0]],
      output: [0, 0, [0, 0]],
    },
    0: {
      input: [0, 0, [0, 0]],
      output: [0, 0, [0, 0]],
    },
  };
  supprShortTable();
  createShortTable();
  modifyShortTable();
};

function createShortTable() {
  let table = document.createElement("table");
  table.id = "shortTable";
  document.getElementById("shortTableContainer").appendChild(table);
  addHeadingShort();
}

function supprShortTable() {
  let tmp = document.getElementById("shortTable");
  if (tmp != undefined)
    tmp.parentNode.removeChild(tmp);
}

function addHeadingShort() {
  let table = document.getElementById("shortTable");
  let tableRow = document.createElement("tr");
  let itemCol = null;
  for (let i = 0; i < colShort; i++) {
    itemCol = document.createElement("th");
    itemCol.scope = "col";
    itemCol.innerText = arrayHeadingShort[i];
    if (i == 3) {
      itemCol.colSpan = 2;
    } else {
      itemCol.rowSpan = 2;
    }
    tableRow.appendChild(itemCol);
  }

  table.appendChild(tableRow);
  tableRow = document.createElement("tr");
  for (let i = 1; i != 3; i++) {
    itemCol = document.createElement("th");
    itemCol.scope = "col";
    itemCol.innerText = "Byte " + i;
    tableRow.appendChild(itemCol);
  }
  table.appendChild(tableRow);
}

function modifyShortTable() {
  let table = document.getElementById("shortTable");
  let tableRow;

  for (let i = MAX_FLOOR; i != -1; i--) {
    tableRow = document.createElement("tr");

    if (i % 2 === 0) {
      tableRow.style.backgroundColor = "rgba(133,156,255,0.5)";
    } else {
      tableRow.style.backgroundColor = "white";
    }

    let itemCol = document.createElement("td");
    itemCol.scope = "col";
    itemCol.innerText = i;
    itemCol.rowSpan = 2;
    tableRow.appendChild(itemCol);

    addRow(tableRow, i, "input");
    table.appendChild(tableRow);

    tableRow = document.createElement("tr");
    if (i % 2 === 0) {
      tableRow.style.backgroundColor = "rgba(133,156,255,0.5)";
    } else {
      tableRow.style.backgroundColor = "white";
    }
    addRow(tableRow, i, "output");
    table.appendChild(tableRow);
  }
}

function addRow(row, floor, type) {
  let itemCol = document.createElement("td");

  if (showInHexa)
    itemCol.innerText = arrayDataShort[floor][type][0].toString(16).toUpperCase();
  else
    itemCol.innerText = hexaToBin(arrayDataShort[floor][type][0].toString(2).toUpperCase(), 12);
  row.appendChild(itemCol);

  itemCol = document.createElement("td");
  itemCol.scope = "col";
  itemCol.innerText = arrayDataShort[floor][type][1].toString(16).toUpperCase();
  row.appendChild(itemCol);


  for (let i = 0; i != 2; i++) {
    itemCol = document.createElement("td");
    itemCol.scope = "col";
    if (showInHexa)
      itemCol.innerText = arrayDataShort[floor][type][2][0].toString(16).toUpperCase();
    else
      itemCol.innerText = hexaToBin(arrayDataShort[floor][type][2][0].toString(2).toUpperCase(), 12);
    row.appendChild(itemCol);
  }
}

function reloadShortTable() {
  supprShortTable();
  createShortTable();
  modifyShortTable();
}

function addShortTable(data) {
  let floorID;
  if ((floorID = getFloorID[data[2] - defaultCANId["input"]]) != undefined) {
    arrayDataShort[floorID]["input"][0] = data[2];
    arrayDataShort[floorID]["input"][1] = data[3];
    arrayDataShort[floorID]["input"][2] = data[4];
    reloadShortTable();
  } else if ((floorID = getFloorID[data[2] - defaultCANId["output"]]) != undefined) {
    arrayDataShort[floorID]["output"][0] = data[2];
    arrayDataShort[floorID]["output"][1] = data[3];
    arrayDataShort[floorID]["output"][2] = data[4];
    reloadShortTable();
  }
}