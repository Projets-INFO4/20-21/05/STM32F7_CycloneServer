window.addEventListener("load", function () {
    initSketch();
});

const MAX_FLOOR = 4;
const IMAGES = "images/";

let cabin = {
    floor: 0,
    bis: 0,
};




//doorState : CLOSE | CLOSING_OPENING | OPEN
function addFloor(cabinIsHere, floorNumber, doorState) {
    let table = document.getElementById('sketchTable');
    let row = document.createElement('div');
    row.classList.add("flexHorizontalContainer","raw")
    let containerDoorSide = document.createElement('div');
    containerDoorSide.classList.add("flexHorizontalContainer");

    let left = document.createElement('div');
    left.classList.add("flexVerticalContainer","floor","floorDoor");
    let indicatorUpFloorImage = document.createElement('img');
    indicatorUpFloorImage.classList.add("floorIndicator", "buttonsAndLight");
    addImage(indicatorUpFloorImage,"IndicatorOFF","indicator floor up image", "png", "indicatorUF" + floorNumber);
    let indicatorFloorImage = document.createElement('img');
    indicatorFloorImage.classList.add("floorIndicator", "buttonsAndLight");
    addImage(indicatorFloorImage,"IndicatorOFF","indicator floor image", "png", "indicatorF" + floorNumber);
    let indicatorDownFloorImage = document.createElement('img');
    indicatorDownFloorImage.classList.add("floorIndicator", "buttonsAndLight");
    addImage(indicatorDownFloorImage,"IndicatorOFF","indicator floor down image", "png", "indicatorDF" + floorNumber);
    left.append(indicatorFloorImage,indicatorUpFloorImage,indicatorDownFloorImage);

    let doorContainer = document.createElement("div");
    doorContainer.classList.add("flexVerticalContainer","doorContainer");
    let doorImage = document.createElement('img');
    doorImage.classList.add("doorImage");
    addImage(doorImage, "Door" + doorState, "door image" + doorState, "jpg", "imageDoor" + floorNumber);

    let direction = document.createElement('img');
    direction.classList.add("doorMovementImage");
    addImage(direction, "CLOSE", "no direction", "png", "imageDirection" + floorNumber);

    doorContainer.append(direction,doorImage)
    containerDoorSide.append(left, doorContainer);


    let floorCell = document.createElement('div');
    floorCell.classList.add("floor","floorNumber");
    floorCell.innerText = floorNumber;

    let containerUserSide = document.createElement('div');
    containerUserSide.classList.add("floor","floorButton");
    let bpImage = document.createElement('img');
    bpImage.classList.add("userSide","buttonsAndLight","button");
    bpImage.setAttribute('onclick','pressedButtonOnFloor(' + floorNumber + ')');
    addImage(bpImage, "BpOFF", "pressButton", "png", "imagePressButtonFloor" + floorNumber);
    let indicatorImage = document.createElement('img');
    indicatorImage.classList.add("userSide","buttonsAndLight","light");   
    addImage(indicatorImage, "IndicatorOFF", "Indicator of the press button", "png", "indicatorBP" + floorNumber);
    containerUserSide.appendChild(bpImage);
    containerUserSide.appendChild(indicatorImage);

    row.append(containerDoorSide, floorCell, containerUserSide);
    table.appendChild(row);
}


function addCabin(){
    let containerLeft = document.createElement("cabinButtons");
    containerLeft.classList.add("flexVerticalContainer","cabinButtons");
    for (let i=MAX_FLOOR; i>=0; i--) {
        let buttonCabin = document.createElement("img");
        buttonCabin.setAttribute('onclick','pressedButtonCabin(' + i + ')');
        buttonCabin.classList.add("cabinButtonsImage");
        addImage(buttonCabin, "BpOFF", "Button cabin to floor " + i, "png", "buttonCabin" + i);
        containerLeft.appendChild(buttonCabin);
    }

    
    let containerRight = document.getElementById("drcIndicators");
   
    containerRight.classList.add("flexHorizontalContainer");
    for (let i=0; i<MAX_FLOOR+1; i++) {
        let drcIndicator = document.createElement("img");
        drcIndicator.classList.add("lightCabin");
        addImage(drcIndicator, "IndicatorOFF", "drc indicator floor " + i, "png", "drcIndicator" + i);
        containerRight.appendChild(drcIndicator);
    }

    let cabinCell = document.getElementById("cabinSpace");
    let cabin = document.createElement('div');
    let cabinAndLigth = document.createElement('div');
    cabinAndLigth.classList.add("cabinAndLight")
    cabin.id="cabin";
    let cabinImage = document.createElement('img');
    cabinImage.classList.add("cabinImage");
    cabin.classList.add("cabin");
    addImage(cabinImage, "Cabin", "cabin image", "jpg", "imageCabinFloor");

    cabin.style.top="80%";
    cabinAndLigth.appendChild(containerRight);
    cabinAndLigth.appendChild(cabinImage);
    cabin.appendChild(containerLeft);
    cabin.appendChild(cabinAndLigth);
    cabinCell.appendChild(cabin);
    cabinCell.classList.add("containerCabin");
}



let minFloorToPixels=80; // in %
let maxFloorToPixels=2;  // in %
let keepAnimation=false;
let timeForOneFloor=1; // in s
let fps=60;


function checkAnimationCabin(floor,upDownSensor){
    let elevatorCabin=document.getElementById("cabin");
    
    let position;

    if(floor-cabin.floor>=2 || floor-cabin.floor<=-2){
        cabin.floor=floor;
        cabin.bis=upDownSensor;
        keepAnimation=false;
        position=-1/3*upDownSensor;
        elevatorCabin.style.top=""+(maxFloorToPixels+(MAX_FLOOR-floor)*(minFloorToPixels-maxFloorToPixels)/MAX_FLOOR+(minFloorToPixels-maxFloorToPixels)/MAX_FLOOR*position)+"%";
    }else{
        let facteur=cabin.floor-floor;
        position = getPosition(facteur,upDownSensor);
        if(facteur==0 && position!=0){
            if(upDownSensor==0){
                facteur=cabin.bis;
            }
            else facteur= -upDownSensor;
        }
        cabin.floor=floor;
        cabin.bis=upDownSensor;
        keepAnimation=true;
        animationCabin(facteur,0,position);
    }
}

function getPosition(facteur,sensorPos){
    let pos=1;
    if(sensorPos==0){
        if(cabin.bis==-1){
            if(facteur!=0){
                pos+=-1/3*facteur
            }else{
                pos=1/3;
            }
        }else if(cabin.bis==1){
            if(facteur!=0){
                pos+=1/3*facteur
            }else{
                pos=1/3;
            }
        }else if(facteur==0)pos=0;
    }else if(sensorPos==1){
        if(cabin.bis==-1){
            if(facteur!=0){
                pos+=-0.66*facteur
            }else{
                pos=0.66;
            }
        }else if(cabin.bis==0){
            if(facteur!=0){
                pos+=-1/3*facteur
            }else{
                pos=1/3;
            }
        }else if(facteur==0)pos=0;
    }else if(sensorPos==-1) {
        if(cabin.bis==1){
            if(facteur!=0){
                pos+=0.66*facteur
            }else{
                pos=0.66;
            }
        }else if(cabin.bis==0){
            if(facteur!=0){
                pos+=1/3*facteur
            }else{
                pos=1/3;
            }
        }else if(facteur==0)pos=0;
    }
    return pos;
}

function animationCabin(facteur,counterAnimation,position){
    if(!keepAnimation || counterAnimation>=fps*timeForOneFloor*position)return;
    let elevatorCabin=document.getElementById("cabin");
    let tmp=""+(Number(elevatorCabin.style.top.replace("%",""))+facteur*((minFloorToPixels-maxFloorToPixels)/MAX_FLOOR)/(fps*timeForOneFloor))+"%";
    if(keepAnimation)elevatorCabin.style.top=tmp;
    counterAnimation++;
    setTimeout(()=> {
        animationCabin(facteur,counterAnimation,position);
    },1000/fps)
}


function changeImageDoorFloor(floor, state) {
    let newFloor = document.getElementById("imageDoorFloor" + floor);
    addImage(newFloor, "Door" + state, "image state" + state, "jpg", newFloor.id);
}

// state ON / OFF
function changeImagePressButton(floor, state) {
    let pressButton = document.getElementById('imagePressButtonFloor' + floor);
    addImage(pressButton, "Bp" + state, "image pressButton" + state, "png", pressButton.id);
}

function initSketch() {
    for (let i = MAX_FLOOR; i >= 0; i--) {
        if (i == 0)
            addFloor(true, i, "CLOSE");
        else
            addFloor(false, i, "CLOSE");
    }
    addCabin()
}

// ext : extension of the image : jpeg
// src : path from folder IMAGES of the image with the name of the file without the ext
function addImage(node, src, alt, ext, id) {
    node.src = IMAGES + src + "." + ext;
    node.alt = alt;
    node.id = id;
}