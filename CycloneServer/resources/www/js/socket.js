let socket = null;
let isStarted;

function newWebSocket() {
    let menuItem = document.getElementById("startStop");
    socket = new WebSocket("ws://" + window.location.host);
    socket.addEventListener("open", function () {
      console.log("Websocket connection established");
      isStarted = true;
      menuItem.innerText = "Close websocket";
      loadFull();
      loadShort();
    });
  
    socket.addEventListener("error", function () {
      alert(
        "A websocket error occurred, there are probably no more websocket connections available"
      );
      console.log("A websocket error occurred");
      isStarted = false;
      menuItem.innerText = "Start websocket";
    });
  
    socket.addEventListener("close", function () {
      console.log("Websocket connection closed");
      isStarted = false;
      menuItem.innerText = "Start websocket";
    });
  
    socket.addEventListener("message", function (event) {
      console.log("Websocket message received: ", event.data);
      dataReceived(event.data);
    });
  }

  window.addEventListener("unload", function () {
    if (socket.readyState == WebSocket.OPEN) {
      closeConnection();
    }
  });
  
  function startLisenSocket() {
    if (socket.readyState == WebSocket.OPEN)
      alert(
        "Unable to start a websocket connection while you already have one running..."
      );
    else if (socket.readyState == WebSocket.CLOSING)
      alert(
        "Unable to start a websocket connection while you are closing one..."
      );
    else if (socket.readyState == WebSocket.CONNECTING)
      alert(
        "Unable to start a websocket connection while you already are connecting to one..."
      );
    else {
      let menuItem = document.getElementById("startStop");
      menuItem.innerText = "Starting websocket...";
      isStarted = true;
      newWebSocket();
    }
  }
  
  function stopListenSocket() {
    if (socket.readyState == WebSocket.OPEN) {
      let menuItem = document.getElementById("startStop");
      menuItem.innerText = "Closing websocket...";
      closeConnection();
      isStarted = false;
    } else if (socket.readyState == WebSocket.CLOSING)
      alert("The websocket connection is still closing...");
    else if (socket.readyState == WebSocket.CONNECTING)
      alert("A websocket connection is in creation...");
    else alert("Unable to stop websocket, no one is activated...");
  }
  
  function startStop() {
    if (isStarted)
      stopListenSocket();
    else
      startLisenSocket();
  }
  
  function closeConnection() {
    socket.send("close");
    socket.close();
  }