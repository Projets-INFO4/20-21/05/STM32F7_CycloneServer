# Documentation of this project

## What do you need?
- STM32F779I-EVAL board
- STM32CubeIDE
- An internet box or a computer with an ethernet port
- An ethernet cable (RJ45)

## How to make the server work?
1. Add the project **CycloneServer** in STM32CubeIDE
2. Build and flash with STM32CubeIDE the program into a STM32F779I-EVAL board
3. Make sure jumpers are in the good position (JP3, JP4, JP6, JP8 and JP12):<p align="center"><img src="docs/jumpers.jpg" alt="jumpers"></p>
4.  - If you want to connect it directly to a box, there is nothing to do
    - If you want to connect it to a computer, first follow these steps:
        + Windows 10:
            1. Create a static IP address (Control Panel -> Network and Internet -> Network and Sharing Center -> Change adapter settings -> Right-click and Properties on the network adapter -> Select Internet Protocol Version 4 (TCP/IPv4) -> Properties)<p align="center"><img src="docs/staticIPAddressW10.jpg" alt="staticIPAddressW10"></p>
            2. Download and launch: [TFTPD](http://tftpd32.jounin.net/tftpd32_download.html)
            3. Fill information<p align="center"><img src="docs/tftpdSettings.jpg" alt="tftpdSettings"></p>
            4. Connect the board into the computer
        + Ubuntu:
            1. Create a static IP address (Settings -> Network -> Select your interface)<img src="docs/staticIPAddressU.jpg" alt="staticIPAddressU"></p>
            2. Download isc-dhcp-server: ```sudo apt install isc-dhcp-server```
            3. Copy and past into */etc/dhcp/dhcpd.conf*:
                ```
                # Sample /etc/dhcpd.conf
                # (add your comments here) 
                default-lease-time 600;
                max-lease-time 7200;
                option subnet-mask 255.255.255.0;
                option routers 192.168.1.10;
                option domain-name-servers 8.8.8.8;
                
                subnet 192.168.10.0 netmask 255.255.255.0 {
                range 192.168.10.210 192.168.10.230;
                }
                ```
            4. Add your ethernet interface into */etc/default/isc-dhcp-server*<img src="docs/ethernetInterface.jpg" alt="ethernetInterface"></p>
            5. Execute ```sudo service isc-dhcp-server restart```
5. Connect to the IP address and it should works! (when you start the server, it may take a bit long before you can access the webpage)
<p align="center"><img src="docs/screen.jpg" alt="screen"></p>
<p align="center"><img src="docs/webpage.jpg" alt="webpage"></p>

## How to make the CAN work?
**PART OF IESE3 STUDENTS BUT NOT DONE YET...**

## How to change the webpage?
1. Put into your **resource/** directory your files: .html, .css, .js...
2. You might change ```strcpy(httpServerSettings.rootDirectory, "/www/");``` and ```strcpy(httpServerSettings.defaultDocument, "index.html");``` in your function main in **main.c** with your own root directory and your own home page
3. You might change your functions ```httpServerCgiCallback```, ```httpServerRequestCallback``` and ```httpServerUriNotFoundCallback``` in **main.c** to match what you want to do with your website
4. It is done! You do not have to do anything else because when you build or flash the code, it will build automatically the **resource/** folder

## Structure of our main.c
### Our basic functions
- headerLCD: function to print our header on the LCD
- footerLCD: function to print our footer on the LCD
- logoLCD: function to print different logos on the LCD
- addrLCD: function to print the IPv4 Address of the board (or _CONNECTING..._ if it does not have one)
- sendCANBusToClient: function to format CAN data into XML and send it to all websocket connections available
- parseMessage: function which parse a char buffer with structure:
    - If the server receives CAN data from client:
      ```<data>
	  <id>xx</id>
	  <B1>xx</B1>
	  <B2>xx</B2>
	  </data>
    - If the server receives a close request (to close the websocket connection)
      ```close```

### Our server functions
- httpServerCgiCallback: function if you want to use Common Gateway Interface in your website
- httpServerRequestCallback: function for HTTP request callback (when the URI is correct)
- httpServerUriNotFoundCallback: function called when a non-existing URI is used

### Our CAN functions
**PART OF IESE3 STUDENTS BUT NOT DONE YET...**

### Our tasks
- lcdTask: task which call _addrLCD_
- canTask: task which check if a new CAN message is here, recovers it in variables _RxHeader_ and _RxData_ and sends it to all websocket connections open using _sendCANBusToClient_ (only if the id of the message is between 0x182 - 0x186 and 0x202 - 0x206 because they are those which are interesting in our case of the elevator)
- clientTask: task which watches if a client sent a message, and if it is the case, parse it using _parseMessage_
    - close the connection if the message is ```close```
    - parse the CAN message if the message is not ```close```

### Our main
Initialisation of our board with LCD configuration, CAN configuration, Ethernet configuration and our tasks

## Folders structure
- Core: main programs and configuration files
- Drivers: drivers for STM32
- Middleware:
    - Cyclone: all CycloneTCP code (library)
    - FreeRTOS: code of freertos
    - ST: code of components we use (for example LCD)
    - resources: client code (html, css, js)

## Client
- Sketch:
    - You can interact with all buttons which will send data to CAN bus through the server<p align="center"><img src="docs/sketch.jpg" alt="sketch"></p>
- Full table:
    - You can see all data received
    - You can convert data into hexadecimal or binary
    - You can filter data (id or byte1 or byte2: id==0x-- or byte1==0x-- or byte2==0x-- where -- is the value you want to see)<p align="center"><img src="docs/full.jpg" alt="full table"></p>
- Short table:
    - You can see last data received for each floor (input and output mode)
    - You can convert data into hexadecimal or binary<p align="center"><img src="docs/short.jpg" alt="short table"></p>