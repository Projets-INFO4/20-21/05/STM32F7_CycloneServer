# Embedded Server using CycloneTCP library

# Overview
This project is an embedded server (using a library named _CycloneTCP_) which communicate with CAN bus for STM32F779I-EVAL boards.

## Team
- Tutor: Didier DONSEZ
- Students:
    - INFO4 (server part):
        - Liam ANDRIEUX
        - Lucas DREZET
        - Roman REGOUIN
    - IESE3 (CAN part):
        - Léo BARBET
        - Nicolas LYSEE
        - Yoan MOREAU

## Documentation:
- [STM32 and website part](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer/-/blob/master/DOCUMENTATION.md "Documentation STM32")
- [General part](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/Documentation/-/blob/master/README.md "General documentation")
